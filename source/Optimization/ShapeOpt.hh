#ifndef SHAPEOPT_HH_
#define SHAPEOPT_HH_

#include <stddef.h>
#include <string>

#include "General/defs.hh"
#include "MatVec/Matrix.hh"
#include "Optimization/ParamMat.hh"

namespace CoupledField {

  /** This class implements shape optimization, it uses lots of things from Ersatzmaterial
   * and only overrides everything concerning derivative calculation
   * it uses ShapeDesign instead of the standard design space, there most of the actual work is done
   * it inherites from ParamMat to be able to use shape & param concurrently */
class Condition;
class Excitation;
class Function;
class Objective;
class ShapeDesign;
template <class TYPE> class Vector;

  class ShapeOpt : public ParamMat {
  public:

    ShapeOpt();

    /** Overwritten version of ErsatzMaterial::CalcVolume */
    virtual double CalcVolume(Objective* f, Condition* g, bool derivative, bool normalized = true);

    /** Calculate part of the derivative u1 dK/dShape u2 of compliance and tracking w.r.t. shape.
     * This is done using the isoparametric approach as described in Haslinger and Diss of Bastian Schmidt */
    void CalcMinusU1dKU2(StateContainer& forward, StateContainer& adjoint, Objective* f, Condition* constraint, const Matrix<double>* tensor_diff = NULL);
    
    /** Calculate part of the derivative u dF/dShape of compliance and tracking w.r.t. shape. This part is only implemented for pressure. */
    void CalcUdF(StateContainer& adjoint, Objective* f, Condition* constraint, double w = 1.0);

    /** Overwritten version of ErsatzMaterial::CalcCompliance */
    virtual double CalcCompliance(Excitation& excite, Objective* f, Condition* constraint, bool derivative);

    /** Overwritten version of ErsatzMaterial::CalcTracking */
    virtual double CalcTracking(Excitation& excite, Objective* f, Condition* constraint, bool derivative);
    
    /** Overwritten version of ErsatzMaterial::CalcHomogenizedTrackingGradient */
    virtual void CalcHomogenizedTrackingGradient(const Matrix<double>& target, const Matrix<double>& hom, Function* f);
    
    /** Overwritten version of ErsatzMaterial::CalcHomogenizedTensor */
    virtual Matrix<double> CalcHomogenizedTensor(Function* f);

    /** Pointer to the DesignSpace, with the correct type */
    ShapeDesign* shapedesign;
    
  protected:
    /** Store the results from the forward/adjoint problem. Handles multiple excitations
     * @param read_sol only if true do s.th. if false just pass on to ersatzmaterial
     * @param read_rhs is only interesting for the forward problem
     * @param save_sol set this in the adjoint problem -> see Solution::Read()
     * @param comment is just to LOG_DBG */
    virtual void StorePDESolution(StateContainer& solutions, Excitation &excite, Function* f, UInt timestep, bool read_sol, bool read_rhs, bool save_sol, TimeDeriv derivative, const std::string& comment);

    /** Subtract the current Testdisplacement from a given vector, needed by CalcHomogenizedTensor 
     * @param idx index of the excitation 
     * @param CornerCoords coordinates of the current element 
     * @param result element displacement vector from which the testdisplacement should be subtracted 
     * @param tmp_strain temporary variable for speed 
     * @param tmp_displacement temporary varibale for speed */
    inline void SubtractTestDisplacement(unsigned int idx, Matrix<double>& CornerCoords, Vector<double>& result, Matrix<double>& tmp_strain, Matrix<double>& tmp_displacement);

  private:

    /** whether material is also optimized */
    bool exoprt_fe_design_;
  };

}

#endif /*SHAPEOPT_HH_*/
